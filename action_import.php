<?php
include('common_bo.php');

$data = json_decode($_REQUEST['data'], true);
$res = \ImportAlexGrupp\Import::import($data);

$res['message'] = implode('<br>', $res['messages']);
unset($res['messages']);

echo json_encode($res);