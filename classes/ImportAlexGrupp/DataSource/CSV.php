<?php

namespace ImportAlexGrupp\DataSource;

class CSV extends DataSourceAbstract
{
    public $fields;
    protected $fh;
    protected $data = [
        'offset' => 0
    ];

    public static function setup()
    {
        $t = \Cetera\Application::getInstance()->getTranslator();

        return array(
            array(
                'xtype' => 'fileselectfield',
                'fieldLabel' => $t->_('Файл'),
                'allowBlank' => false,
                'name' => 'source_file'
            ),
            array(
                'xtype' => 'textfield',
                'fieldLabel' => $t->_('Кодировка'),
                'allowBlank' => false,
                'name' => 'source_charset',
                'value' => 'windows-1251',
            ),
            array(
                'xtype' => 'radio',
                'checked' => true,
                'fieldLabel' => $t->_('Разделитель полей'),
                'boxLabel' => 'точка с запятой',
                'name' => 'source_delimiter',
                'inputValue' => ';',
            ),
            array(
                'xtype' => 'radio',
                'hideEmptyLabel' => false,
                'boxLabel' => $t->_('запятая'),
                'name' => 'source_delimiter',
                'inputValue' => ',',
            ),
            array(
                'xtype' => 'radio',
                'hideEmptyLabel' => false,
                'boxLabel' => $t->_('табуляция'),
                'name' => 'source_delimiter',
                'inputValue' => '\t',
            ),
            array(
                'xtype' => 'checkbox',
                'fieldLabel' => $t->_('Первая строка содержит имена полей'),
                'checked' => true,
                'name' => 'source_skip_first',
                'inputValue' => 1,
            ),
        );
    }

    public function getFields()
    {
        if (empty($this->fields)) {
            $this->setFields();
        }

        $charset = $this->params['source_charset'];
        $hasFieldLabels = $this->params['source_skip_first'];
        $result = [];

        foreach ($this->fields as $id => $field) {
            $id += 1;
            $result[] = [
                'id' => $field,
                'label' => $hasFieldLabels ?
                    sprintf("Поле %d (%s)", $id, $field) :
                    sprintf("Поле %d", $id)
            ];
        }

        return $result;
    }

    public function setFields()
    {
        $delimiter = $this->params['source_delimiter'];

        $this->openFile();

        if ($this->params['source_skip_first']) {
            $row = fgetcsv($this->fh, 0, $delimiter);
            $charset = $this->params['source_charset'];
            if (is_array($row)) {
                foreach ($row as $i => $field) {
                    $this->fields[] = iconv($charset, 'utf-8', $field);
                }
            }
        } else {
            $maxRowLength = 0;
            while ($row = fgetcsv($this->fh, 0, $delimiter)) {
                $rowLength = count($row);
                if ($rowLength > $maxRowLength) {
                    $maxRowLength = $rowLength;
                }
            }
            for ($i = 0; $i < $maxRowLength; $i++) {
                $this->fields[] = $i;
            }
        }
        rewind($this->fh);
    }

    protected function openFile()
    {
        if (!$this->fh) {
            $file = WWWROOT . $this->params['source_file'];
            if (!file_exists($file)) throw new \Exception('File ' . $this->params['file'] . ' is not found.');
            $this->fh = fopen($file, 'r');
        }
    }

    protected function closeFile()
    {
        if (is_resource($this->fh)) {
            fclose($this->fh);
        }
    }

    public function fetch()
    {
        if ($this->data['offset'] > ftell($this->fh)) {
            fseek($this->fh, $this->data['offset']);
        }

        $row = fgetcsv($this->fh, 0, $this->params['source_delimiter']);

        if (!$this->data['offset'] && $this->params['source_skip_first']) {
            $row = fgetcsv($this->fh, 0, $this->params['source_delimiter']);
        }

        $result = [];
        if (is_array($row)) {
            $rowLength = count($row);
            for ($i = 0; $i < $rowLength; $i++) {
                $result[$this->fields[$i]] = iconv($this->params['source_charset'], 'utf-8', $row[$i]);
            }
        } else {
            $this->eof = true;
        }

        $this->data['offset'] = ftell($this->fh);
        return $result;
    }

}