<?php

namespace ImportAlexGrupp;

class Import
{
    use \Cetera\DbConnection;

    private static $config;
    private static $res;
    private static $objectDefs;
    private static $dataSource;
    private static $iterator;
    private static $dbConnection;
    private static $colors;

    public static function executeTemplate($id, $counter = 0)
    {
        $data = self::getDbConnection()
            ->fetchAssoc('SELECT * FROM import_alex_grupp_templates WHERE `id` = ?', [$id]);

        if ($data) {
            $params = unserialize($data['data']);
            $params['counter'] = $counter;
            return self::import($params);
        } else {
            throw new \Exception('Template ' . $id . ' not found');
        }
    }

    public static function import($params)
    {
        foreach ($params as $key => $value) {
            $params[$key] = array_filter($value, function ($val) {
                $type = gettype($val);
                return !empty($val) && ($type === 'string' || $type === 'integer');
            });
        }

        self::$config['setup'] = $params['setup'];
        self::$config['filters'] = array_filter($params['filters'], function ($filter) {
            return gettype($filter) === 'string';
        });

        $fields = array_flip($params['fields']);
        $setup = self::$config['setup'];

        self::$dataSource = new self::$config['setup']['data_source'](self::$config['setup']);
        $dataSource = self::$dataSource;

        self::$config['current_user'] = \Cetera\Application::getInstance()->getUser()->id;
        self::$dbConnection = self::getDbConnection();

        self::$objectDefs['product_params'] = \Cetera\ObjectDefinition::findByAlias('sale_products_params');
        self::$objectDefs['pictures'] = \Cetera\ObjectDefinition::findByAlias('gallery_picture');
        self::$objectDefs['sub'] = \Cetera\ObjectDefinition::findByAlias('sale_offers');
        self::$objectDefs['main'] = \Cetera\ObjectDefinition::getById(self::$config['setup']['materials_type']);
        $od = self::$objectDefs['main'];
        self::$config['rootCatalog'] = \Cetera\Catalog::getById(self::$config['setup']['catalog']);

        self::$res = [
            'success' => true,
            'counter' => $setup['counter'],
            'messages' => $setup['messages']
        ];

        // delete or unpublish materials before import
        self::beforeImportDo($setup['missing']);

        self::$res['_current_line'] = (int)$setup['_current_line'];
        self::$iterator = (int)$setup['_iteration'];

        if (!self::$res['_iteration']) {
            self::$res['messages'][] = 'Обработка файла данных ...';
        }

        $dataSource->setFields();

        while (!$dataSource->eof) {
            self::$iterator += 1;
            self::$config['current_option'] = [];
            $row = $dataSource->fetch();

            if ($row) {
                if (!self::doesCorrespondWithFilters($row)) {
                    continue;
                }

                $searchField = $setup['unique_field'];
                if (!empty($row[$fields[$searchField]])) {
                    // for root material
                    if (empty($row[$fields['parent_code']])) {
                        $data = self::populate($row, $fields, $od);
                        $data['alias'] .= '-' . $data['code'];

                        $material = self::getMaterialByFieldValue($searchField, $data[$searchField]);

                        if ($material) {
                            $data = array_merge($material->fields, $data);
                            $material->setFields($data);
                            $material->save();
                            self::$config['rootMaterial'] = $material;
                        } else {
                            self::$config['rootMaterialData'] = $data;
                            self::$config['rootMaterial'] =
                                \Cetera\DynamicFieldsObject::fetch($data, self::$objectDefs['main']);
                            self::$config['rootMaterial']->save();
                        }
                        self::triggerImportEvent(self::$config['rootMaterial'],
                            self::$config['fields'], self::$config['setup']);
                    } else {
                        // for sub materials
                        $data = self::populate($row, $fields, self::$objectDefs['sub']);
                        $data['name'] = implode(', ', self::$config['current_option']);
                        $data['alias'] .= '-' . $data['code'];

                        $material = self::$objectDefs['sub']->getMaterials()->where("{$searchField}=:{$searchField}")
                            ->setParameter($searchField, $data[$searchField])->current();

                        if ($material) {
                            $data = array_merge($material->fields, $data);
                            $material->setFields($data);
                        } else {
                            $material = \Cetera\DynamicFieldsObject::fetch($data, self::$objectDefs['sub']);
                        }

                        $material->save();
                        self::triggerImportEvent($material, self::$config['fields'], self::$config['setup']);
                    }
                }
            }
        }

        if ($dataSource->eof) {
            if (strpos(self::$res['messages'][count(self::$res['messages']) - 1], " ОК") === false) {
                self::$res['messages'][count(self::$res['messages']) - 1] .= ' OK';
            }
            self::$iterator += 1;
        }

        if ($setup['missing'] === 'delete') {
            self::$res['messages'][] = 'Удаление материалов ...';
            self::$res['_iteration'] += 1;

            $list = self::$config['rootCatalog']->getMaterials()->unpublished()->subFolders()->where('type=50');

            foreach ($list as $m) {
                $m->delete();
            }
            self::$res['messages'][count(self::$res['messages']) - 1] .= ' OK';
        }

        self::$res['messages'][] = 'Импорт завершен.';
        return self::$res;
    }

    private static function triggerImportEvent($material, $fields, $params)
    {
        \Cetera\Event::trigger('IMPORT_MATERIAL_AFTER_IMPORT', [
            'material' => $material,
            'fields' => $fields,
            'params' => $params
        ]);
    }

    /**
     * @param array $row
     * @param array $fields
     * @param \Cetera\ObjectDefinition $objectDef
     * @return array
     * @throws \Exception
     */
    private static function populate(Array $row, Array $fields, \Cetera\ObjectDefinition $objectDef)
    {
        $dataDefaults = [
            'name' => 'Импортированный материал ' . self::$iterator,
            'publish' => 1,
            'type' => 1,
            'autor' => self::$config['current_user'],
            'currency' => \Sale\Currency::getDefault(),
            'idcat' => -1,
            'discount' => 0
        ];

        $data = [];

        foreach ($fields as $materialField => $sourceField) {
            $currFieldValue = trim($row[$sourceField]);
            if (!empty($materialField) && !empty($currFieldValue)) {
                if ($materialField === 'idcat') {
                    if (gettype($currFieldValue) === 'string') {
                        if ($currFieldValue === self::$config['currentCatalog']->name) {
                            $idCat = self::$config['currentCatalog']->id;
                        } else {
                            self::$config['currentCatalog'] = self::getCatalogByName($currFieldValue);
                            $idCat = self::$config['currentCatalog']->id;
                        }

                        if (isset($fields['subCatalog_1'])) {
                            $path = [];
                            for ($i = 1; $i <= 3; $i++) {
                                $sub = $fields['subCatalog_' . $i];
                                if ($row[$sub]) {
                                    $path[] = trim($row[$sub]);
                                    unset($row[$sub]);
                                }
                            }
                            $idCat = self::getSubCatId($path);
                        }

                        $data[$materialField] = $idCat;
                    }
                } else if ($materialField === 'parent_code') {
                    $data['product'] = self::$config['rootMaterial']->id;
                } else if ($materialField === 'pictures') {
                    $productName = translit($data['name']) . '-';

                    if (empty($row[$fields['parent_code']])) {
                        $productName .= $data[self::$config['setup']['unique_field']];
                        if (strpos($currFieldValue, 'http') !== false) {
                            $picData = [
                                'idcat' => -1,
                                'alias' => 'hidden',
                                'autor' => self::$config['current_user'],
                            ];

                            if (strpos($currFieldValue, '|')) {
                                $imageUrls = array_filter(explode('|', $currFieldValue));
                                $imageUrl = array_shift($imageUrls);
                                $data['pic'] = self::fetchImage($imageUrl, $productName);
                                foreach ($imageUrls as $imageUrl) {
                                    $picData['file'] = self::fetchImage($imageUrl, $productName);
                                    $pic = \Cetera\DynamicFieldsObject::fetch($picData, self::$objectDefs['pictures']);
                                    $pic->save();
                                    $data['pictures'][] = $pic->id;
                                }
                            } else {
                                $data['pic'] = self::fetchImage($currFieldValue, $productName);
                            }
                        } else {
                            $data['pic'] = $currFieldValue;
                        }
                    } else {
                        $productName .= $row[$fields['parent_code']];
                        if (strpos($currFieldValue, 'http') !== false) {
                            if (strpos($currFieldValue, '|')) {
                                $currFieldValue = explode('|', $currFieldValue)[0];
                            }
                            $data['pic'] = self::fetchImage($currFieldValue, $productName);
                        } else {
                            $data['pic'] = $currFieldValue;
                        }
                    }
                } else {
                    try {
                        $fieldDef = $objectDef->getField($materialField);
                        switch ($fieldDef['type']) {

                            case FIELD_INTEGER:
                                $currFieldValue = preg_replace("/[^0-9\-]/", "", $currFieldValue);
                                $data[$materialField] = intval($currFieldValue);
                                break;

                            case FIELD_DOUBLE:
                                $currFieldValue = preg_replace("/[^0-9\.,\-]/", "", $currFieldValue);
                                $currFieldValue = str_replace(',', '.', $currFieldValue);
                                $data[$materialField] = floatval($currFieldValue);
                                break;

                            case FIELD_LINK:
                                $data[$materialField] = self::getLinkValue($fieldDef, $currFieldValue);
                                break;

                            case FIELD_LINKSET:
                                if (strpos($currFieldValue, '|') !== false) {
                                    $currFieldValue = explode('|', $currFieldValue);
                                }

                                $data[$materialField] = self::getLinkSetValue($fieldDef, $currFieldValue, $materialField);
                                break;

                            case FIELD_FILE:
                                if (is_array($currFieldValue)) {
                                    if (!isset($currFieldValue['filename'])) {
                                        break;
                                    }
                                    $data[$materialField] = USER_UPLOAD_PATH . 'import/' . $currFieldValue['filename'];
                                    if (!file_exists(WWWROOT . $data[$materialField]) && $currFieldValue['data']) {
                                        if (!file_exists(WWWROOT . USER_UPLOAD_PATH . 'import')) {
                                            mkdir(WWWROOT . USER_UPLOAD_PATH . 'import',
                                                0777, true);
                                        }
                                        file_put_contents(WWWROOT . $data[$materialField], $currFieldValue['data']);
                                    }
                                } else {
                                    $data[$materialField] = $currFieldValue;
                                }
                                break;

                            default:
                                if (isset($data[$materialField])) {
                                    $data[$materialField] .= "\n" . $currFieldValue;
                                } else {
                                    $data[$materialField] = $currFieldValue;
                                }
                        }
                    } catch (\Exception $e) {
                    }
                }
            }
        }

        $data = array_merge($dataDefaults, $data);

        if (!$data['alias']) {
            $data['alias'] = translit($data['name']);
        }

        return $data;
    }

    private static function beforeImportDo($action)
    {
        if ($action === 'unpublish') {
            self::$dbConnection->executeUpdate(
                'UPDATE ' . self::$objectDefs['main']->table . ' SET type=0 WHERE idcat IN (?)',
                [self::$config['rootCatalog']->getSubs()],
                [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]
            );

        } else if ($action === 'delete') {
            self::$dbConnection->executeUpdate(
                'UPDATE ' . self::$objectDefs['main']->table . ' SET type=50 WHERE idcat IN (?)',
                [self::$config['rootCatalog']->getSubs()],
                [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]
            );
        }

        self::$res['counter'] += 1;
        self::$res['messages'][] = 'Деактивация материалов ... ОК';
    }

    private static function prepareTextField($field, $delimiter)
    {
        $field = str_replace($delimiter, '', $field);

        if (strpos($field, $delimiter) !== false) {
            $field = self::prepareTextField($field, $delimiter);
        }

        return $field;
    }

    private static function getSubCatId($path)
    {
        $currentCat = self::$config['currentCatalog'];
        foreach ($path as $catName) {
            $newCat = $currentCat->getChildren()->where("name='$catName'")->current();
            if (empty($newCat->name)) {
                throw new \Exception("Не смог найти подраздел \"$catName\" в каталоге \"$currentCat->name\"");
            } else {
                $currentCat = $newCat;
            }
        }
        return $currentCat->id;
    }

    private static function checkForMultibyteChars($string)
    {
        return !mb_check_encoding($string, 'ASCII') && mb_check_encoding($string, 'UTF-8');
    }

    private static function fetchImage($url, $productName)
    {
        set_time_limit(0);
        $pathInfo = pathinfo(parse_url($url)['path']);
        $fileName = $pathInfo['filename'];
        $ext = $pathInfo['extension'];
        if (self::checkForMultibyteChars($fileName)) {
            $fileName = translit($fileName);
        }
        $fileName = "{$fileName}.{$ext}";
        $uploadPath = USER_UPLOAD_PATH . 'import/' . $productName . '/';
        if (!file_exists(DOCROOT . $uploadPath)) {
            mkdir(DOCROOT . $uploadPath, 0777, true);
        }
        $uploadPath .= $fileName;
        $filePath = DOCROOT . $uploadPath;

        if (!file_exists($filePath) || self::$config['setup']['override_images']) {
            $file = fopen($filePath, 'w+');
            $ch = curl_init(str_replace(" ", "%20", $url));
            curl_setopt($ch, CURLOPT_TIMEOUT, 50);
            curl_setopt($ch, CURLOPT_FILE, $file);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_exec($ch);
            curl_close($ch);
            fclose($file);
        }

        return $uploadPath;
    }

    private static function doesCorrespondWithFilters($row)
    {
        if (empty(self::$config['filters'])) {
            return true;
        }

        foreach (self::$config['filters'] as $id => $filter) {
            if (is_array($row[$id])) {
                $value = self::flattenArray($row[$id]);
            } else {
                $value = (string)$row[$id];
            }

            try {
                $match = preg_match($filter, $value);
            } catch (\Exception $e) {
                $match = substr_count($value, $filter);
            }

            if (!$match) {
                return false;
            }
        }
        return true;
    }

    private static function getMaterialByFieldValue($field, $value)
    {
        $table = self::$objectDefs['main']->table;
        $id = self::$dbConnection->fetchAssoc("SELECT id FROM $table WHERE $field = $value")['id'];

        if ($id) {
            return \Cetera\Material::getById($id, $table);
        }

        return null;
    }

    private static function getCatalogByName($name)
    {
        if (empty(self::$config['catalogs'])) {
            self::$config['catalogs'] = self::$objectDefs['main']->getCatalogs();
        }

        foreach (self::$config['catalogs'] as $cat) {
            if ($cat->name === $name) {
                return $cat;
            }
        }

        throw new \Exception("Не смог найти раздел: $name, с типом материалов: "
            . self::$objectDefs['main']->id);
    }

    public static function getLinkValue($fieldDef, $value)
    {
        $i = $fieldDef->getCatalog()->getMaterials()->where('name="' . $value . '"')->subfolders()->current();
        if ($i) {
            self::$config['current_option'][$fieldDef->name] = $i->name;
            return $i->id;
        }

        $data = [
            'name' => $value,
            'idcat' => $fieldDef->getCatalog()->id,
            'autor' => self::$config['current_user'],
            'alias' => translit($value),
            'publish' => 1,
        ];

        $m = \Cetera\DynamicFieldsObject::fetch($data, $fieldDef->getCatalog()->getMaterialsObjectDefinition());
        $m->save();

        self::$config['current_option'][$fieldDef->name] = $m->name;

        return $m->id;
    }

    public static function getLinkSetValue($fieldDef, $value, $param_type)
    {
        if (!is_array($value)) {
            $value = array($value);
        }

        $data = array();

        foreach ($value as $v) {
            $v = trim($v);
            $i = $fieldDef->getCatalog()->getMaterials()->where('name="' . $v . '"')->subfolders();
            if ($i->getCountAll() > 0) {
                $data[] = $i->current()->id;
                continue;
            }

            $alias = translit($v);

            $material = $fieldDef->getCatalog()->getMaterials()
                ->where('alias = :alias')->setParameter('alias', $alias)->current();

            if ($material) {
                $alias .= '-' . rand(100, 999);
            }

            $newParamData = [
                'name' => $v,
                'idcat' => $fieldDef->getCatalog()->id,
                'autor' => self::$config['current_user'],
                'alias' => $alias,
                'publish' => 1,
                'param_type' => $param_type,
            ];

            if ($param_type === 'color' && strpos($v, '/') !== false) {
                $colorNames = explode('/', $v);
                $colorHexes = [];
                foreach ($colorNames as $colorName) {
                    $hex = self::$objectDefs['product_params']->getMaterials()
                        ->where("param_type = 'color' and name = :name")->setParameter('name', $colorName)
                        ->current()->color;
                    if ($hex) {
                        $colorHexes[] = $hex;
                    }
                }

                if (!empty($colorHexes)) {
                    $colorHexes = implode('|', $colorHexes);
                } else {
                    $colorHexes = '';
                }

                $newParamData['param_type'] = $param_type;
                $newParamData['color'] = $colorHexes;
            }

            $m = \Cetera\DynamicFieldsObject::fetch($newParamData, $fieldDef->getCatalog()->getMaterialsObjectDefinition());
            $m->save();
            $data[] = $m->id;

        }

        return json_encode($data);
    }

    public static function getTemplate($name)
    {
        $data = self::getDbConnection()->fetchAssoc('SELECT * FROM import_alex_grupp_templates WHERE `name` = ?', [$name]);

        if ($data) {
            return unserialize($data['data']);
        } else {
            return false;
        }
    }

    protected static function flattenArray($data)
    {
        if (!is_array($data)) {
            return (string)$data;
        }

        $values = [];
        foreach ($data as $key => $item) {
            if ((string)$key == 'id') continue;
            $values[] = self::flattenArray($item);
        }
        return implode('//', $values);
    }
}
