<?php
// Подключаем каталог с переводами модуля
$t = $this->getTranslator();
$t->addTranslation(__DIR__ . '/lang');

\ImportAlexGrupp\DataSourceIterator::add([
    'id' => '\ImportAlexGrupp\DataSource\CSV',
    'name' => $t->_('Файл CSV')
]);

if ($this->getBo()) {
    $this->getBo()->addModule(array(
        'id' => 'importAlexGrupp',
        'position' => MENU_SITE,
        'name' => $t->_('Импорт AlexGrupp'),
        'icon' => 'import.gif',
        'class' => 'Plugin.import_alex_grupp.Panel',
    ));
}

