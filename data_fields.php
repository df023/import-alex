<?php
include('common_bo.php');
$t = $application->getTranslator();

$data = [
    ['id' => '', 'name' => '[' . $t->_('пропустить') . ']'],
    ['id' => 'publish', 'name' => $t->_('Активность')],
    ['id' => 'color', 'name' => 'Цвет'],
    ['id' => 'parent_code', 'name' => 'Родительский материал (parent_code)'],
    ['id' => 'pictures', 'name' => 'Изображения (pictures)'],
    ['id' => 'subCatalog_1', 'name' => $t->_('Название подраздела - Уровень 1')],
    ['id' => 'subCatalog_2', 'name' => $t->_('Название подраздела - Уровень 2')],
    ['id' => 'subCatalog_3', 'name' => $t->_('Название подраздела - Уровень 3')],
];

$r = $application->getConn()->query('SELECT field_id as id, name, describ, type from types_fields where id=' . (int)$_REQUEST['type_id'] . ' order by tag');
while ($f = $r->fetch()) {

    if ($f['type'] == FIELD_MATSET) continue;

    if (in_array($f['name'], array('alias', 'tag', 'dat_update', 'type'))) continue;

    $data[] = array(
        'id' => $f['name'],
        'name' => 'Поле "' . $application->decodeLocaleString($f['describ']) . '" (' . $f['name'] . ')'
    );
}

echo json_encode(array(
    'success' => true,
    'rows' => $data
));