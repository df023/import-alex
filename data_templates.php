<?php
include('common_bo.php');

$conn = \Cetera\Application::getInstance()->getDbConnection();

if ($_REQUEST['action'] == 'destroy') {
    $data = json_decode(file_get_contents("php://input"), true);

    $conn->delete('import_alex_grupp_templates', array(
        'id' => $data['id']
    ));
    echo json_encode(array(
        'success' => true,
    ));
    die();
}

$query = $conn->createQueryBuilder();
$query->select('*')->from('import_alex_grupp_templates');

$stmt = $query->execute();
while ($row = $stmt->fetch()) {
    $params = unserialize($row['data']);
    $params['material_type'] = (int)$params['material_type'];
    $fields = [];
    $setup = [];
    foreach ($params as $id => $value) {
        if (substr($id, 0, 6) == 'field_' || substr($id, 0, 7) == 'filter_') {
            $fields[$id] = $value;
        } else {
            $setup[$id] = $value;
        }
    }
    $data[] = array(
        'id' => $row['id'],
        'name' => $row['name'],
        'setup' => $setup,
        'fields' => $fields
    );
}

echo json_encode(array(
    'success' => true,
    'rows' => $data
));